#!/bin/bash

#SBATCH -c 8
#SBATCH -N 1

#SBATCH -p priority
#SBATCH --job-name STAR_genomeGen
#SBATCH --mem 45gb
#SBATCH --time 0-3:00

project_dir=HF_LV
script_dir=script_hf_lv

GTF=/home/yl477/Genome_files/human_genome/gencode.v39.primary_assembly.annotation.gtf
fasta=/home/yl477/Genome_files/human_genome/GRCh38.primary_assembly.genome.fa
#GTF=/home/yl477/Genome_files/mouse_genome/gencode.vM24.primary_assembly.annotation.gtf
#fasta=/home/yl477/Genome_files/mouse_genome/GRCm38.primary_assembly.genome.fa


module load gcc/6.2.0
module load star/2.7.9a

mkdir /n/scratch3/users/y/yl477/${project_dir}/STAR_genome_len150
STAR --runThreadN 8 --runMode genomeGenerate --genomeDir /n/scratch3/users/y/yl477/${project_dir}/STAR_genome_len150 --genomeFastaFiles ${fasta} --sjdbGTFfile ${GTF} --sjdbOverhang 149

echo "done"