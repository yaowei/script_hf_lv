#!/usr/bin/env bash

# This script takes a fastq file of RNA-Seq data, runs FastQC and outputs a counts file for it.
# USAGE: sh rnaseq_analysis_on_allfiles.sh <name of fastq file>

# initialize a variable with an intuitive name to store the name of the input fastq file
project_dir=HF_LV
script_dir=script_hf_lv

fq1=$1
fq2=$2

# grab base of filename for naming outputs

filename=$(basename -- "$fq1")
base="${filename%.fastq.gz}"
echo "Sample name is $base"

# Run FastQC and move output to the appropriate folder
/home/yl477/software/TrimGalore/trim_galore --fastqc_args "--outdir /n/scratch3/users/y/yl477/${project_dir}/fastqc_trim/" --gzip --paired -j 8 -o /n/scratch3/users/y/yl477/${project_dir}/trim_galore/ ${fq1} ${fq2}
