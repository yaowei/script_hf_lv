#!/usr/bin/env bash
project_dir=HF_LV
script_dir=script_hf_lv

mkdir /n/scratch3/users/y/yl477/${project_dir}/fastqc_trim
mkdir /n/scratch3/users/y/yl477/${project_dir}/trim_galore
source /programs/biogrids.shrc

for fq1 in /n/scratch3/users/y/yl477/${project_dir}/fastq/*_1.fastq.gz; do
fq2=$(echo $fq1 | sed 's/_1.fastq.gz/_2.fastq.gz/g');

sbatch -p short -t 0-8:00 -n 8 --mem 16G --job-name rnaseq-trim --wrap="sh /n/scratch3/users/y/yl477/${project_dir}/${script_dir}/rna-seq-trim.sh $fq1 $fq2"
sleep 1	# wait 1 second between each job submission

done
