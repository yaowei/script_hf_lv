﻿#!/bin/bash

#SBATCH -c 2
#SBATCH -N 1

#SBATCH -p priority
#SBATCH --job-name download
#SBATCH --mem 8gb
#SBATCH --time 0-32:00

curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586435/SRR8586435_1.fastq.gz -o SRR8586435_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586435/SRR8586435_2.fastq.gz -o SRR8586435_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586438/SRR8586438_1.fastq.gz -o SRR8586438_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586438/SRR8586438_2.fastq.gz -o SRR8586438_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586433/SRR8586433_1.fastq.gz -o SRR8586433_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586433/SRR8586433_2.fastq.gz -o SRR8586433_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586437/SRR8586437_1.fastq.gz -o SRR8586437_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586437/SRR8586437_2.fastq.gz -o SRR8586437_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586436/SRR8586436_1.fastq.gz -o SRR8586436_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586436/SRR8586436_2.fastq.gz -o SRR8586436_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586434/SRR8586434_1.fastq.gz -o SRR8586434_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586434/SRR8586434_2.fastq.gz -o SRR8586434_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/001/SRR8586431/SRR8586431_1.fastq.gz -o SRR8586431_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/001/SRR8586431/SRR8586431_2.fastq.gz -o SRR8586431_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586432/SRR8586432_1.fastq.gz -o SRR8586432_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586432/SRR8586432_2.fastq.gz -o SRR8586432_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/009/SRR8586429/SRR8586429_1.fastq.gz -o SRR8586429_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/009/SRR8586429/SRR8586429_2.fastq.gz -o SRR8586429_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586428/SRR8586428_1.fastq.gz -o SRR8586428_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586428/SRR8586428_2.fastq.gz -o SRR8586428_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586427/SRR8586427_1.fastq.gz -o SRR8586427_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586427/SRR8586427_2.fastq.gz -o SRR8586427_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/000/SRR8586430/SRR8586430_1.fastq.gz -o SRR8586430_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/000/SRR8586430/SRR8586430_2.fastq.gz -o SRR8586430_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586426/SRR8586426_1.fastq.gz -o SRR8586426_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586426/SRR8586426_2.fastq.gz -o SRR8586426_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586425/SRR8586425_1.fastq.gz -o SRR8586425_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586425/SRR8586425_2.fastq.gz -o SRR8586425_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586422/SRR8586422_1.fastq.gz -o SRR8586422_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586422/SRR8586422_2.fastq.gz -o SRR8586422_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/001/SRR8586421/SRR8586421_1.fastq.gz -o SRR8586421_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/001/SRR8586421/SRR8586421_2.fastq.gz -o SRR8586421_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586423/SRR8586423_1.fastq.gz -o SRR8586423_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586423/SRR8586423_2.fastq.gz -o SRR8586423_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586424/SRR8586424_1.fastq.gz -o SRR8586424_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586424/SRR8586424_2.fastq.gz -o SRR8586424_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/000/SRR8586420/SRR8586420_1.fastq.gz -o SRR8586420_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/000/SRR8586420/SRR8586420_2.fastq.gz -o SRR8586420_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586418/SRR8586418_1.fastq.gz -o SRR8586418_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586418/SRR8586418_2.fastq.gz -o SRR8586418_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586415/SRR8586415_1.fastq.gz -o SRR8586415_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586415/SRR8586415_2.fastq.gz -o SRR8586415_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/009/SRR8586419/SRR8586419_1.fastq.gz -o SRR8586419_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/009/SRR8586419/SRR8586419_2.fastq.gz -o SRR8586419_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586416/SRR8586416_1.fastq.gz -o SRR8586416_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586416/SRR8586416_2.fastq.gz -o SRR8586416_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586417/SRR8586417_1.fastq.gz -o SRR8586417_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586417/SRR8586417_2.fastq.gz -o SRR8586417_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586414/SRR8586414_1.fastq.gz -o SRR8586414_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586414/SRR8586414_2.fastq.gz -o SRR8586414_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586413/SRR8586413_1.fastq.gz -o SRR8586413_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586413/SRR8586413_2.fastq.gz -o SRR8586413_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586412/SRR8586412_1.fastq.gz -o SRR8586412_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586412/SRR8586412_2.fastq.gz -o SRR8586412_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/001/SRR8586411/SRR8586411_1.fastq.gz -o SRR8586411_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/001/SRR8586411/SRR8586411_2.fastq.gz -o SRR8586411_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/000/SRR8586410/SRR8586410_1.fastq.gz -o SRR8586410_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/000/SRR8586410/SRR8586410_2.fastq.gz -o SRR8586410_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/009/SRR8586409/SRR8586409_1.fastq.gz -o SRR8586409_human_HCM_patients_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/009/SRR8586409/SRR8586409_2.fastq.gz -o SRR8586409_human_HCM_patients_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586408/SRR8586408_1.fastq.gz -o SRR8586408_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/008/SRR8586408/SRR8586408_2.fastq.gz -o SRR8586408_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586407/SRR8586407_1.fastq.gz -o SRR8586407_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/007/SRR8586407/SRR8586407_2.fastq.gz -o SRR8586407_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586406/SRR8586406_1.fastq.gz -o SRR8586406_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/006/SRR8586406/SRR8586406_2.fastq.gz -o SRR8586406_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586405/SRR8586405_1.fastq.gz -o SRR8586405_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/005/SRR8586405/SRR8586405_2.fastq.gz -o SRR8586405_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586403/SRR8586403_1.fastq.gz -o SRR8586403_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/003/SRR8586403/SRR8586403_2.fastq.gz -o SRR8586403_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586404/SRR8586404_1.fastq.gz -o SRR8586404_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/004/SRR8586404/SRR8586404_2.fastq.gz -o SRR8586404_human_healthy_donors_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586402/SRR8586402_1.fastq.gz -o SRR8586402_human_healthy_donors_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR858/002/SRR8586402/SRR8586402_2.fastq.gz -o SRR8586402_human_healthy_donors_2.fastq.gz