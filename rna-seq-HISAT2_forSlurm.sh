#!/usr/bin/env bash

project_dir=HF_LV
script_dir=script_hf_lv

mkdir ../bam

for fq1 in /n/scratch3/users/y/yl477/${project_dir}/trim_galore/*_1_val_1.fq.gz; do
fq2=$(echo $fq1 | sed 's/_1_val_1.fq.gz/_2_val_2.fq.gz/g');

sbatch -p short -t 0-8:00 -c 8 --mem 16G --job-name HISAT2 --wrap="sh /n/scratch3/users/y/yl477/${project_dir}/${script_dir}/rna-seq-HISAT2.sh $fq1 $fq2"
sleep 1	# wait 1 second between each job submission

done