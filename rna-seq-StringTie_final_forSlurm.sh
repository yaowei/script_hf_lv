#!/usr/bin/env bash

source /programs/biogrids.shrc
mkdir ../StringTie

project_dir=HF_LV
script_dir=script_hf_lv

for bam in /n/scratch3/users/y/yl477/${project_dir}/bam/*.bam; do

sbatch -p short -t 0-4:00 -c 8 --mem 16G --job-name StringTie-H --wrap="sh /n/scratch3/users/y/yl477/${project_dir}/${script_dir}/rna-seq-StringTie_final.sh $bam"
sleep 1	# wait 1 second between each job submission
done